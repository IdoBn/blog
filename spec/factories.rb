FactoryGirl.define do

	factory :comment do
		content { Faker::Lorem.sentences.join(' ') }
	end

	factory :post do |f|
		f.title { Faker::Lorem.sentence }
		f.body { Faker::Lorem.sentences.join(' ') }
	end
	
end