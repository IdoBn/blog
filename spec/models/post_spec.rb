require 'spec_helper'

describe Post do
	let(:comment) { FactoryGirl.create(:comment) }
	subject(:post) { FactoryGirl.build(:post) }

	it { should be_an_instance_of(Post) }

	it { expect { subject.save }.to change { Post.count }.by(1) }

	it 'includes comments' do
		post.comments << comment
		post.comments.should include(comment)
	end
end
