require 'spec_helper'

describe Comment do
	subject(:comment) { FactoryGirl.create(:comment) }
	let(:post) { FactoryGirl.build(:post) }

	it { should be_an_instance_of(Comment) }

	it { expect { subject.save }.to change { Comment.count }.by(1) }

	it 'is dependant destroy' do
		post.comments << comment
		expect { post.destroy }.to change { Comment.count }.by(-1)
	end
end