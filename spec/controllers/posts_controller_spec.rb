require 'spec_helper'

describe PostsController do
	let(:valid_attributes) { FactoryGirl.attributes_for(:post) }
	context 'GET #index' do
		subject(:post) { FactoryGirl.create(:post) }
		it 'assigns @posts' do
			get :index
			expect(assigns(:posts)).to eq([post])
		end

		it 'renders the :index view' do
			get :index
			response.should render_template :index
		end
	end

	context 'GET #show' do
		subject(:post) { FactoryGirl.create(:post) }
		it 'assigns @post' do
			get :show, id: post.id
			assigns(:post).should eq(post)
		end

		it 'renders the :show view' do
			get :show, id: post
			response.should render_template :show
		end
	end

	context 'GET #new' do
		it 'assigns @post' do
			get :new
			assigns(:post).should be_an_instance_of(Post)
		end

		it 'renders the :new view' do
			get :new
			response.should render_template :new
		end
	end

	context 'POST #create' do
		describe 'with valid attributes' do
			it 'creates a new post' do
				expect {
					post :create, post: valid_attributes
				}.to change { Post.count }.by(1)
			end

			it 'redirects to the new post' do
				post :create, post: valid_attributes
				response.should redirect_to Post.last
			end
		end

		describe 'with invalid attributes' do
			it 'does not save the new post' do
				Post.any_instance.stub(:valid?).and_return(false)
				expect {
					post :create, post: valid_attributes
				}.to_not change { Post.count }
			end

			it 're-renders the new method' do
				Post.any_instance.stub(:valid?).and_return(false)
				post :create, post: valid_attributes
				response.should render_template :new
			end
		end
	end	

	context 'PUT #update' do
		before :each do
			@post = FactoryGirl.create(:post)
		end

		describe 'valid attributes' do
			it 'found @post' do
				put :update, id: @post, post: valid_attributes
				assigns(:post).should eq(@post)
			end

			it 'updated @post' do
				put :update, id: @post, post: FactoryGirl.attributes_for(:post, title: "hello", body: "world")
				@post.reload
				@post.title.should eq("hello")
				@post.body.should eq("world")
			end

			it 'redirects to the updated post' do
				put :update, id: @post, post: valid_attributes
				response.should redirect_to @post
			end
		end

		describe 'invalid attributes' do
			it 'found @post' do
				put :update, id: @post, post: valid_attributes
				assigns(:post).should eq(@post)
			end

			it 'does not update @post' do
				Post.any_instance.stub(:valid?).and_return(false)
				put :update, id: @post, post: FactoryGirl.attributes_for(:post, title: "goodbye", body: "cruel world")
				@post.reload
				@post.title.should_not eq("goodbye")
				@post.body.should_not eq("cruel world")
			end

			it 're-renders the edit method' do
				Post.any_instance.stub(:valid?).and_return(false)
				put :update, id: @post, post: valid_attributes
				response.should render_template :edit
			end
		end
	end

	context 'DELETE #destroy' do
		before :each do
			@post = FactoryGirl.create(:post)
		end

		it 'deletes the post' do
			expect {
				delete :destroy, id: @post
			}.to change { Post.count }.by(-1)
		end

		it 'redirects to index' do
			delete :destroy, id: @post
			response.should redirect_to posts_url
		end
	end
end