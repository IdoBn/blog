require 'spec_helper'

describe CommentsController do
	context 'GET #show' do
		let(:post) { FactoryGirl.create(:post) }
		subject(:comment) { FactoryGirl.create(:comment) }

		it 'assigns @comment' do
			post.comments << comment
			get :show, id: comment.id, post_id: comment.post_id
			assigns(:comment).should eq(comment)
		end

		it 'renders the :show view' do
			post.comments << comment
			get :show, id: comment, post_id: comment.post_id
			response.should render_template :show
		end
	end

	context 'GET #new' do
		let(:post) { FactoryGirl.create(:post) }
		it 'assigns @comment and @post' do
		 	get :new, post_id: post.id
		 	assigns(:post).should be_an_instance_of(Post)
		end

		it 'renders the :new view' do
		 	get :new, post_id: post.id
			response.should render_template :new
		end
	end

	context 'POST #create' do
		let(:valid_attributes) { FactoryGirl.attributes_for(:comment) }
		let(:post1) { FactoryGirl.create(:post) }

		describe 'with valid attributes' do
			it 'creates a new comment' do
				expect {
					post :create, comment: valid_attributes, post_id: post1.id
				}.to change { post1.comments.count }.by(1)
			end

			it 'redirects to the post' do
				post :create, comment: valid_attributes, post_id: post1.id
				response.should redirect_to post1
			end
		end

		describe 'with invalid attributes' do
			it 'does not save the new comment' do
				# needs work
			end

			it 're-renders the new method' do
				Comment.any_instance.stub(:valid?).and_return(false)	
				post :create, comment: valid_attributes, post_id: post1.id
				response.should render_template :new	
			end
		end
	end
end